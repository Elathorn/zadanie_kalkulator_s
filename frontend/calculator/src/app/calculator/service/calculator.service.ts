import { Injectable, forwardRef, NgModule } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { Country } from '../model/Country';
import { Money } from '../model/Money';

@NgModule({providers: [forwardRef(() => CalculatorService)]})
export class FooModule {
}

@Injectable()
export class CalculatorService {
    private API_URL = "http://localhost:8092/calculator"

    constructor(private http: Http) {}

    calculate(amount: string, country: string): Observable<Money> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let params = new URLSearchParams();
        params.set('dailyGrossAmount', amount);
        params.set('country', country);

        return this.http
            .get(this.API_URL, {headers: headers, params: {
                dailyGrossAmount: amount,
                country: country
            }})
            .map(res => res.json());
    }
}
