export interface Money {
    amount: string,
    currency: string
}