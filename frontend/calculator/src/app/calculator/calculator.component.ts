import { Component, OnInit } from '@angular/core';

import { CalculatorService } from "./service/calculator.service"

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css'],
  providers: [
    CalculatorService
  ]
})
export class CalculatorComponent implements OnInit {
  plnAmount = "0";
  grossDailyAmount: string;
  countries = ["PL", "GB", "DE"]
  country: string = this.countries[0];

  constructor(private calculatorService : CalculatorService) { }

  setGrossDailyAmount(newValue: string) {
    this.grossDailyAmount = newValue
  }

  setCountry(newValue: string) {
    this.country = newValue
  }

  ngOnInit() {
  }

  calculate() {
    this.calculatorService.calculate(this.grossDailyAmount, this.country).subscribe(response => {
      this.plnAmount = response.amount
    })
  }
}
