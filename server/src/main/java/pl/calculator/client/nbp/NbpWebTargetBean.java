package pl.calculator.client.nbp;

import lombok.Data;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Configuration
@Data
public class NbpWebTargetBean {
    @Value("${nbp.url}")
    private String url;

    @Value("${nbp.timeout}")
    private Integer timeout;

    @Bean
    public WebTarget NbpWebTarget() {
        return ClientBuilder
                .newClient()
                .target(url);
    }

    private ClientConfig clientConfig() {
        return new ClientConfig()
                .property(ClientProperties.CONNECT_TIMEOUT, timeout)
                .property(ClientProperties.READ_TIMEOUT, timeout);

    }
}
