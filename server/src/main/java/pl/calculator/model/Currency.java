package pl.calculator.model;

public enum Currency {
    PLN, GBP, EUR
}
