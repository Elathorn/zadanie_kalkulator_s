package pl.calculator.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@AllArgsConstructor
@Data
public class Rate {
    @JsonProperty("mid")
    private BigDecimal midRate;
}
