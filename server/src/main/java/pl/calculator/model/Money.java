package pl.calculator.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Money {
    private BigDecimal amount;
    private Currency currency;

    public Money(BigDecimal amount, Currency currency) {
        this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        this.currency = currency;
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Money))return false;
        Money otherMoney = (Money) other;
        return (otherMoney.amount.equals(amount) && otherMoney.currency.equals(currency));
    }
}
