package pl.calculator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.calculator.model.Country;
import pl.calculator.model.Currency;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidCountryException extends RuntimeException {
    public InvalidCountryException(Country country) {
        super(String.format("Country code %s is not supported.", country));
    }
}
