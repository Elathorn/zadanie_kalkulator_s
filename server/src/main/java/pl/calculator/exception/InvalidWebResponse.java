package pl.calculator.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.calculator.model.Country;

import java.io.IOException;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class InvalidWebResponse extends RuntimeException {
    public InvalidWebResponse(String message) {
        super(message);
    }

    public InvalidWebResponse(String message, IOException exception) {
        super(message, exception);
    }
}