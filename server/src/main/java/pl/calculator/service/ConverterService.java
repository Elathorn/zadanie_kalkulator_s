package pl.calculator.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.calculator.client.nbp.NbpWebTargetBean;
import pl.calculator.exception.InvalidWebResponse;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;
import pl.calculator.service.strategy.StrategyFactory;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

@Service
public class ConverterService {
    private WebTarget nbpWebTarget;
    private ObjectMapper objectMapper;

    @Autowired
    public ConverterService(WebTarget nbpWebTarget) {
        this.nbpWebTarget = nbpWebTarget;
        this.objectMapper = new ObjectMapper();
    }

    public Money convertToPLN(Money money) {
        BigDecimal rates = getRatesForCurrency(money.getCurrency());
        return new Money(money.getAmount().multiply(rates), Currency.PLN);
    }

    BigDecimal getRatesForCurrency(Currency currency) {
        Response response = nbpWebTarget
                .path("/" + currency)
                .request()
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatusInfo().toEnum() != Response.Status.OK) {
            throw new InvalidWebResponse(
                    "Couldn't get success status from nbp server. Return message:" + response.readEntity(String.class));
        }

        String json = response.readEntity(String.class);
        try {
            return getRateFromJson(json);
        } catch (IOException e) {
            throw new InvalidWebResponse("Couldn't parse returned json", e);
        }
    }

    BigDecimal getRateFromJson(String json) throws IOException {
        ObjectNode node = objectMapper.readValue(json, ObjectNode.class);
        if (node.get("rates") == null || node.get("rates").get(0) == null
                || node.get("rates").get(0).get("mid") == null) {
            throw new InvalidWebResponse("Lack of currency rate in response");
        }
        String rate = node.get("rates").get(0).get("mid").asText();
        try {
            return new BigDecimal(rate);
        } catch (NumberFormatException e) {
            throw new InvalidWebResponse("Returned rates can't be parsed to bigdecimal");
        }
    }
}
