package pl.calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.calculator.model.Country;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;
import pl.calculator.service.strategy.GrossToNetCalculateStrategy;
import pl.calculator.service.strategy.StrategyFactory;

import java.math.BigDecimal;

@Service
public class CalculatorService {
    private ConverterService converterService;
    private final BigDecimal workdaysInMonth = new BigDecimal("22");

    @Autowired
    public CalculatorService(ConverterService converterService) {
        this.converterService = converterService;
    }

    public Money calculateToPLN(BigDecimal dailyGrossAmount, Country country) {
        BigDecimal grossMontlhy = dailyGrossAmount.multiply(workdaysInMonth);
        Money money = calculate(grossMontlhy, country);

        if (money.getCurrency() != Currency.PLN) {
            return converterService.convertToPLN(money);
        }

        return money;
    }

    Money calculate(BigDecimal dailyGrossAmount, Country country) {
        GrossToNetCalculateStrategy calculateStrategy = StrategyFactory.getGrossToNetStrategy(country);
        return calculateStrategy.calculate(dailyGrossAmount);
    }
}
