package pl.calculator.service.strategy;

import pl.calculator.model.Currency;

import java.math.BigDecimal;

class GBTaxAndConstCostStrategy extends TaxAndConstCostStrategy {
    public GBTaxAndConstCostStrategy() {
        super(new BigDecimal("0.20"), new BigDecimal(600), Currency.GBP);
    }
}
