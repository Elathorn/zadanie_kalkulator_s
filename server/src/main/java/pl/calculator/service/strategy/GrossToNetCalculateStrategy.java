package pl.calculator.service.strategy;

import pl.calculator.model.Money;

import java.math.BigDecimal;

public interface GrossToNetCalculateStrategy {
    Money calculate(BigDecimal dailyGrossAmount);
}
