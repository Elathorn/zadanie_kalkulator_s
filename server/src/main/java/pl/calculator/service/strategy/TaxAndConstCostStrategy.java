package pl.calculator.service.strategy;

import pl.calculator.model.Currency;
import pl.calculator.model.Money;

import java.math.BigDecimal;

class TaxAndConstCostStrategy implements GrossToNetCalculateStrategy {
    private BigDecimal constantConst;
    private BigDecimal taxesPercent;
    private Currency currency;

    public TaxAndConstCostStrategy(BigDecimal taxesPercent, BigDecimal constantConst, Currency currency) {
        this.taxesPercent = new BigDecimal(1).subtract(taxesPercent);
        this.constantConst = constantConst;
        this.currency = currency;
    }

    @Override
    public Money calculate(BigDecimal dailyGrossAmount) {
        dailyGrossAmount = dailyGrossAmount.multiply(taxesPercent); //@todo: trailing zeros
        return new Money(dailyGrossAmount.subtract(constantConst), currency);
    }
}
