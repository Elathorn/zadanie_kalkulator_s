package pl.calculator.service.strategy;

import pl.calculator.model.Currency;

import java.math.BigDecimal;

class DETaxAndConstCostStrategy extends TaxAndConstCostStrategy {
    public DETaxAndConstCostStrategy() {
        super(new BigDecimal("0.20"), new BigDecimal(800), Currency.EUR);
    }
}
