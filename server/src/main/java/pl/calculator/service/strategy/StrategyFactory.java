package pl.calculator.service.strategy;


import pl.calculator.exception.InvalidCountryException;
import pl.calculator.model.Country;

public class StrategyFactory {
    public static GrossToNetCalculateStrategy getGrossToNetStrategy(Country country) {
        switch (country) {
            case PL:
                return new PLTaxAndConstCostStrategy();
            case GB:
                return new GBTaxAndConstCostStrategy();
            case DE:
                return new DETaxAndConstCostStrategy();
            default:
                throw new InvalidCountryException(country);
        }
    }
}
