package pl.calculator.service.strategy;

import pl.calculator.model.Currency;

import java.math.BigDecimal;

class PLTaxAndConstCostStrategy extends TaxAndConstCostStrategy {
    public PLTaxAndConstCostStrategy() {
        super(new BigDecimal("0.19"), new BigDecimal(1200), Currency.PLN);
    }
}
