package pl.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.calculator.model.Country;
import pl.calculator.model.Money;
import pl.calculator.service.CalculatorService;

import java.math.BigDecimal;

@CrossOrigin
@RestController
@RequestMapping("/calculator")
public class CalculatorController {
    private CalculatorService calculatorService;

    @Autowired
    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @GetMapping
    public Money calculate(@RequestParam("dailyGrossAmount") BigDecimal dailyGrossAmount,
                           @RequestParam("country") Country country) {
        return calculatorService.calculateToPLN(dailyGrossAmount, country);
    }
}
