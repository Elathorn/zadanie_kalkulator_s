package pl.calculator.tools.money;

import pl.calculator.model.Currency;
import pl.calculator.model.Money;

import java.math.BigDecimal;

public class MoneyUtils {
    public static Money createMoney(String value) {
        return createMoney(value, Currency.PLN);
    }
    public static Money createMoney(String value, Currency currency) {
        return new Money(new BigDecimal(value), currency);
    }
}
