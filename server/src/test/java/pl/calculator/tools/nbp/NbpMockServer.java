package pl.calculator.tools.nbp;

import com.xebialabs.restito.server.StubServer;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static com.xebialabs.restito.builder.stub.StubHttp.whenHttp;
import static com.xebialabs.restito.semantics.Action.status;
import static com.xebialabs.restito.semantics.Action.stringContent;
import static com.xebialabs.restito.semantics.Condition.alwaysTrue;
import static com.xebialabs.restito.semantics.Condition.startsWithUri;

@Component
public class NbpMockServer extends StubServer {
    @Value("${nbp.url}")
    private String url;

    @Autowired
    public NbpMockServer(NbpMockServerConfig config) {
        super(config.getPortNumber());
    }

    public void mockSuccessful(String currencyRate) {
        whenHttp(this)
                .match(alwaysTrue())
                .then(status(HttpStatus.OK_200), stringContent(getJsonForSuccessful(currencyRate)));
    }

    public void mockUnsuccessful() {
        whenHttp(this)
                .match(alwaysTrue())
                .then(status(HttpStatus.BAD_REQUEST_400));
    }

    public void mockInvalidResponse() {
        whenHttp(this)
                .match(alwaysTrue())
                .then(status(HttpStatus.OK_200), stringContent(getJsonForSuccessful("")));
    }

    public void mockInvalidJson() {
        whenHttp(this)
                .match(alwaysTrue())
                .then(status(HttpStatus.OK_200), stringContent(getJsonForSuccessful("{[]")));
    }

    public void mockLackOfCurrencyRates() {
        whenHttp(this)
                .match(alwaysTrue())
                .then(status(HttpStatus.OK_200), stringContent("{}"));
    }

    private String getJsonForSuccessful(String currencyRate) {
        return String.format("{\n" +
                "    \"table\": \"A\",\n" +
                "    \"currency\": \"dolar amerykański\",\n" +
                "    \"code\": \"USD\",\n" +
                "    \"rates\": [\n" +
                "        {\n" +
                "            \"no\": \"078/A/NBP/2018\",\n" +
                "            \"effectiveDate\": \"2018-04-20\",\n" +
                "            \"mid\": %s\n" +
                "        }\n" +
                "    ]\n" +
                "}", currencyRate);
    }
}
