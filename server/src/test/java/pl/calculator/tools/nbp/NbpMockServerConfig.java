package pl.calculator.tools.nbp;

import lombok.Data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class NbpMockServerConfig {
    @Value("${test.nbp.port}")
    private Integer portNumber;
}
