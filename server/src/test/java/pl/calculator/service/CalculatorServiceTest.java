package pl.calculator.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import pl.calculator.model.Country;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;

import java.math.BigDecimal;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static pl.calculator.tools.money.MoneyUtils.createMoney;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorServiceTest {
    public CalculatorServiceTest() {
        MockitoAnnotations.initMocks(this);
    }

    private ConverterService converterService;
    private CalculatorService calculatorService;

    @Before
    public void beforeEach() {
        converterService = mock(ConverterService.class);
        calculatorService = spy(new CalculatorService(converterService));
    }

    @Test
    public void whenPLN_shouldNotCallConversionService() {
        doReturn(createMoney("2850.00")).when(calculatorService).calculate(any(), any());
        calculatorService.calculateToPLN(new BigDecimal(5000), Country.PL);
        verifyZeroInteractions(converterService);
    }

    @Test
    public void whenNotPLN_shouldCallConversionService() {
        doReturn(createMoney("2850.00", Currency.EUR)).when(calculatorService).calculate(any(), any());
        doReturn(createMoney("8850.00")).when(converterService).convertToPLN(any());

        calculatorService.calculateToPLN(new BigDecimal(5000), Country.DE);
        verify(converterService, times(1)).convertToPLN(any());
    }


}