package pl.calculator.service;

import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.calculator.exception.InvalidWebResponse;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;
import pl.calculator.tools.nbp.NbpMockServer;

import static pl.calculator.tools.money.MoneyUtils.createMoney;

@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest
public class ConverterServiceTest {
    @Autowired
    private ConverterService converterService;
    @Autowired
    private NbpMockServer nbpMockServer;

    @BeforeAllMethods
    public void setUp() {
        nbpMockServer.start();
    }

    @AfterAllMethods
    public void cleanUp() {
        nbpMockServer.stop();
    }

    @Test
    public void convertOnSuccessfulResponse() {
        nbpMockServer.mockSuccessful("4.03");
        Money ret = converterService.convertToPLN(createMoney("100", Currency.EUR));
        Money expected = createMoney("403", Currency.PLN);
        Assert.assertEquals(expected, ret);
    }

    @Test
    public void nonSuccessfulResponse() {
        nbpMockServer.mockUnsuccessful();
        try {
            converterService.convertToPLN(createMoney("1"));
        } catch (InvalidWebResponse e) {
            Assert.assertTrue(e.getMessage().startsWith("Couldn't get success status from nbp server. Return message:"));
        }
    }

    @Test
    public void invalidJson() {
        nbpMockServer.mockInvalidJson();
        try {
            converterService.convertToPLN(createMoney("1"));
        } catch (InvalidWebResponse e) {
            Assert.assertTrue(e.getMessage().startsWith("Couldn't parse returned json"));
        }
    }

    @Test
    public void lackOfCurrencyRatesInResponse() {
        nbpMockServer.mockLackOfCurrencyRates();
        try {
            converterService.convertToPLN(createMoney("1"));
        } catch (InvalidWebResponse e) {
            Assert.assertTrue(e.getMessage().startsWith("Lack of currency rate in response"));
        }
    }

    @Test
    public void invalidRatesFormat() {
        nbpMockServer.mockSuccessful("\"not a currency\"");
        try {
            converterService.convertToPLN(createMoney("1"));
        } catch (InvalidWebResponse e) {
            Assert.assertTrue(e.getMessage().startsWith("Returned rates can't be parsed to bigdecimal"));
        }
    }
}