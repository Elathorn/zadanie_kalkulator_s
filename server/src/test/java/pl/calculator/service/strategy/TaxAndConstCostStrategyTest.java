package pl.calculator.service.strategy;

import org.junit.Assert;
import org.junit.Test;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;

import java.math.BigDecimal;

import static pl.calculator.tools.money.MoneyUtils.createMoney;

public class TaxAndConstCostStrategyTest {

    private TaxAndConstCostStrategy prepareStategy(String taxCost, String constCost) {
        return new TaxAndConstCostStrategy(new BigDecimal(taxCost), new BigDecimal(constCost), Currency.PLN);
    }

    @Test
    public void calculationTest_shouldPass() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0.25", "500");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("250"), ret);
    }

    @Test
    public void bigNumbers_shouldPass() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0.50", "50000");
        Money ret = strategy.calculate(new BigDecimal("1000000000"));
        Assert.assertEquals(createMoney("499950000"), ret);
    }

    @Test
    public void reallyBigNumbers_shouldPass() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0.1", "1000000000000000000");
        Money ret = strategy.calculate(new BigDecimal("1000000000000000000000"));
        Assert.assertEquals(createMoney("899000000000000000000"), ret);
    }

    @Test
    public void noCost() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0.5", "0");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("500"), ret);
    }

    @Test
    public void noTax() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0", "500");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("500"), ret);
    }

    @Test
    public void noTaxNoCost() { //clearly heaven!
        GrossToNetCalculateStrategy strategy = prepareStategy("0", "0");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("1000"), ret);
    }

    @Test
    public void negativeTax() { //even better?
        GrossToNetCalculateStrategy strategy = prepareStategy("-0.1", "0");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("1100"), ret);
    }

    @Test
    public void negativeCost() {
        GrossToNetCalculateStrategy strategy = prepareStategy("0.1", "-500");
        Money ret = strategy.calculate(new BigDecimal("1000"));
        Assert.assertEquals(createMoney("1400"), ret);
    }
}