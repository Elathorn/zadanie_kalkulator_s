package pl.calculator.service.strategy;

import org.junit.Test;
import pl.calculator.model.Country;

import static org.junit.Assert.*;

public class StrategyFactoryTest {
    private void assertReturnTypeForCountry(Country country, Class<?> clazz) {
        assertTrue(StrategyFactory.getGrossToNetStrategy(country).getClass().equals(clazz));
    }

    @Test
    public void forPL_shouldReturnPLTaxStrategy() {
        assertReturnTypeForCountry(Country.PL, PLTaxAndConstCostStrategy.class);
    }

    @Test
    public void forDE_shouldReturnDETaxStrategy() {
        assertReturnTypeForCountry(Country.DE, DETaxAndConstCostStrategy.class);
    }

    @Test
    public void forGB_shouldReturnGBTaxStrategy() {
        assertReturnTypeForCountry(Country.GB, GBTaxAndConstCostStrategy.class);
    }
}