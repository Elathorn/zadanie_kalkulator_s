package pl.calculator.controller;

import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.calculator.model.Country;
import pl.calculator.model.Currency;
import pl.calculator.model.Money;
import pl.calculator.tools.nbp.NbpMockServer;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static pl.calculator.tools.money.MoneyUtils.createMoney;

@RunWith(BeforeAfterSpringTestRunner.class)
@SpringBootTest
public class CalculatorControllerTest {
    @Autowired
    private CalculatorController calculatorController;

    @Autowired
    private NbpMockServer nbpMockServer;

    @BeforeAllMethods
    public void setUp() {
        nbpMockServer.start();
    }

    @AfterAllMethods
    public void cleanUp() throws InterruptedException {
        nbpMockServer.stop();
    }

    @Test
    public void shouldCalculateForPL() {
        Money ret = calculatorController.calculate(new BigDecimal("500"), Country.PL);
        Money expected = createMoney("7710");
        assertEquals(expected, ret);
    }

    @Test
    public void shouldCalculateAndConvertForGER() {
        nbpMockServer.mockSuccessful("4");
        Money ret = calculatorController.calculate(new BigDecimal("500"), Country.DE);
        Money expected = createMoney("32000", Currency.PLN);
        assertEquals(expected, ret);

    }
}